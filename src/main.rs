use crate::callback::Callback;
use crate::mutable::Mutable;
use crate::observable::{Observable, Observer};

mod callback;
mod mutable;
mod observable;

fn main() {
    println!("Hello, world!");

    // Simple stuff
    let mut mutable1 = Mutable::new(2);

    println!("Mutable: {}", mutable1.get_field());
    // 1st increment
    println!("Incrementing");
    mutable1.increment_field();
    println!("Mutable now: {}", mutable1.get_field());
    // 2nd increment
    println!("Again");
    mutable1.increment_field();
    println!("Mutable after 2nd increment: {}", mutable1.get_field());

    // Now try with callbacks
    let closure = || mutable_callback(&mut mutable1);
    let mut callback = Callback::new(closure);

    println!("Incrementing with callback");
    callback.call();
    // println!("Mutable after 1st callback increment: {}", mutable1.get_field());
    /*
     * The ^ above line won't work because get_field must borrow `mutable1` immutably,
     * but because of the next `callback.call()` which still borrows `mutable1` as mutable,
     * it won't be possible
     */

    println!("Again with callback");
    callback.call();

    println!("Mutable after 2nd callback increment: {}", mutable1.get_field());

    // Try with observable
    let mut observer = Observer::new(&mut mutable1);
    let mut observable = Observable::new();
    observable.register(observer);

    // println!("First observable emit on {}", mutable1.get_field());
    observable.emit();
    observable.emit();
    println!("Mutable after 2 observable emits: {}", mutable1.get_field());
}

fn mutable_callback(mutable: &mut Mutable) {
    mutable.increment_field();
}