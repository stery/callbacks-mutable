use crate::mutable::Mutable;

pub(crate) struct Observable<'mutable> {
    observers: Vec<Observer<'mutable>>,
}

impl<'mutable> Observable<'mutable> {
    pub(crate) fn new() -> Observable<'mutable> {
        Observable { observers: Vec::new() }
    }

    pub(crate) fn register(&mut self, observer: Observer<'mutable>) {
        self.observers.push(observer);
    }

    pub(crate) fn emit(&mut self) {
        self.observers.iter_mut()
            .for_each(|observer| observer.notify(1));
    }
}

pub(crate) struct Observer<'mutable> {
    mutable: &'mutable mut Mutable,
}

impl<'mutable> Observer<'mutable> {
    pub(crate) fn new(mutable: &mut Mutable) -> Observer {
        Observer { mutable }
    }
    pub(crate) fn notify(&mut self, i: i32) {
        self.mutable.increment_field();
    }
}