pub(crate) struct Mutable {
    field: i32,
}

impl Mutable {
    pub(crate) fn new(field: i32) -> Mutable {
        Mutable { field }
    }

    pub(crate) fn increment_field(&mut self) {
        self.field = increment(self.field);
    }

    pub(crate) fn get_field(&self) -> &i32 {
        &self.field
    }
}

fn increment(x: i32) -> i32 {
    x + 1
}