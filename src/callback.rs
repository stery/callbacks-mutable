pub(crate) struct Callback<F, R> where F: FnMut() -> R {
    callback: F,
}

impl<F, R> Callback<F, R> where F: FnMut() -> R {
    pub(crate) fn new(callback: F) -> Callback<F, R> {
        Callback { callback }
    }

    pub(crate) fn call(&mut self) -> R {
        (self.callback)()
    }
}